# Dual Speaker Tweeter Mod Magisk by Kafabih

# Changelog

v6.0.1 - Fixed for default volume (reddv1)

v6.0- Initial Release.

# Description 

This modules is specifically designed for the Google Nexus 5x.
It replaces the mixer_path.xml located at /system/etc/ to enable dual speaker mod. This results is made upper earpiece as second speaker for playing sounds. Thanks to sshafranko to made this.

# Author

- sshfranko

# Credits

- reddv1

- Kafabih (To Maintain as Magisk Module)
